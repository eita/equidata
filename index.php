<?php

require ('config.php');

$home = ((!isset($_REQUEST['id']) || !$_REQUEST['id']) && !isset($_REQUEST['brasil']));
$type = "";
if (!$home) {
  if (isset($_REQUEST['id']) && $_REQUEST['id']) {
    $type = (strlen($_REQUEST['id'])>2)
      ? 'municipio'
      : 'estado';
  } else {
    $type = 'brasil';
  }
}

if (!$home) {
  switch ($type) {
    case 'municipio':
    case 'estado':
      $typeTxt = ($type=='municipio')
        ? "município"
        : "estado";
      $title .= str_replace('{typeTxt}',$typeTxt, " - Informações sobre {typeTxt}: ".$_REQUEST['name']);
      break;
    case 'brasil':
      $title .= " - Resumo de dados nacionais";
      break;
  }
}

$requestName = "";
$requestId = "";
if (!$home) {
  switch ($type) {
    case 'municipio':
    case 'estado':
      $requestName = $_REQUEST['name'];
      $requestId = $_REQUEST['id'];
      $iframeUrl = ($type=='municipio')
        ? $metabaseMunicipioUrl
        : $metabaseUFUrl;
      $iframeUrl .= $_REQUEST['id'];
      break;
    case 'brasil':
      $iframeUrl = $metabaseBrasilUrl;
      break;
  }
}
?>
<!DOCTYPE html>
<html<?=($home)?'':' class="results"'?>>
<head>
<title><?=$title?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link rel="apple-touch-icon" sizes="57x57" href="<?=$faviconUrl?>apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?=$faviconUrl?>apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=$faviconUrl?>apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?=$faviconUrl?>apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=$faviconUrl?>apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?=$faviconUrl?>apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?=$faviconUrl?>apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?=$faviconUrl?>apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?=$faviconUrl?>apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?=$faviconUrl?>android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=$faviconUrl?>favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?=$faviconUrl?>favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=$faviconUrl?>favicon-16x16.png">
<link rel="manifest" href="<?=$faviconUrl?>manifest.json">

<!-- METADATA -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?=$faviconUrl?>ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta property="og:url" content='<?=$url?>'>
<meta property="og:type" content='article'>
<meta property="og:title" content='<?=$title?>'>
<meta property="og:description" content='<?=$description?>'>
<meta property="og:image" content='<?=$siteImageUrl?>'>
<meta property="article:author" content="">
<meta property="og:site_name" content="<?=$title?>">
<!--<meta property="article:published_time" content="2014-08-12T00:01:56+00:00">-->
<meta name="twitter:card" content='<?=$description?>'>
<!--<meta name="twitter:site" content='http://agroecologiaemrede.org.br/index.php?'>-->
<!--<meta name="twitter:creator" content="[@yourtwitter]">-->
<meta itemscope itemtype="http://schema.org/Article">
<meta itemprop="headline" content='<?=$title?>'>
<meta itemprop="description" content='<?=$description?>'>
<meta itemprop="image" content='<?=$siteImageUrl?>'>

<link rel="stylesheet" href="assets/bootstrap-4.1.1/dist/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Advent+Pro:300,400,700|Lato:300,300i,400,400i,700,700i" rel="stylesheet">
<link rel="stylesheet" href="assets/open-iconic/font/css/open-iconic-bootstrap.css" type="text/css">
<link rel="stylesheet" href="style.css" type="text/css">
<script src="assets/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="assets/bootstrap-4.1.1/dist/js/bootstrap.min.js"></script>
<script src="assets/jQuery-Bootstrap-4-Typeahead-Plugin/bootstrap3-typeahead.min.js"></script>
<script src="dataTypeahead.js"></script>
<script src="app.js"></script>
</head>
<body<?=($home)?'':' class="results"'?>>
<script>
	var url="<?=$url?>";
</script>
<nav id="menuGeral" class="navbar navbar-expand-md fixed-top navbar-light bg-white">
	<div class="container-fluid">
    <div class="d-none d-md-block">
      <a href="index.php" class="d-none d-lg-inline"><span class="text-titles mr-3">PAINEL</span></a>
  	  <a href="index.php"><img class="mr-3" style="height: 70px" src="<?=$headLogo?>"></a>
      <a href="index.php"><span class="text-title"><?=$headTitle?></span></a>
    </div>
    <div class="d-block d-md-none">
      <a href="index.php"><img class="" style="height: 50px" src="<?=$url?>images/marca-obteia-linhas.png"></a>
    </div>

	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
	    </ul>
      <?php if(!$home) { ?>
        <!--<a href="?brasil=1" class="btn btn-outline-dark d-inline mr-2">BRASIL</a>-->
        <form id="searchForm" class="form-inline" method="GET" action="index.php" autocomplete="off">
    	    <div class="input-group">
    	      <div class="input-group-prepend">
    	        <span class="input-group-text" id="basic-addon1"><i class="oi oi-magnifying-glass"></i></span>
    	      </div>
    	      <input type="text" class="form-control typeahead" placeholder="Estado, UF ou município" aria-label="Estado, UF ou município" aria-describedby="basic-addon1" name="name" value="<?=$requestName?>">
    	    </div>
          <input id="inputId" type="hidden" name="id" value="<?=$requestId?>">
    	  </form>
      <?php } ?>
	  </div>
	</div>
</nav>
<div class="container<?=($home)?'':'-fluid h-100'?>">
  <?php if ($home) { ?>
  	<div class="row mt-4">
      <div class="col-md-12 mt-4">
        <h1><?=$textTitle?></h1>
        <?=$textBody?>
      </div>
  	</div>
  	<div class="row mt-4">
      <div class="col-sm-3 col-md-2 col-lg-1 text-center">
        <a href="?brasil=1" class="btn btn-outline-dark">BRASIL</a>
      </div>
      <div class="col-sm-3 col-md-2 col-lg-1" style="text-align: center;height: 40px;padding-top: 8px;">
        <p>OU</p>
      </div>
      <div class="col-sm-6 col-md-8 col-lg-10">
        <form id="searchForm" class="form-inline" method="GET" action="index.php" autocomplete="off">
          <div class="input-group w-100">
            <div class="input-group-prepend">
              <span class="input-group-text" id="basic-addon1"><i class="oi oi-magnifying-glass"></i></span>
            </div>
            <input type="text" class="form-control typeahead" placeholder="Estado, UF ou município" aria-label="Estado, UF ou município" aria-describedby="basic-addon1" name="name" value="<?=$requestName?>">
          </div>
          <input id="inputId" type="hidden" name="id" value="<?=$requestId?>">
        </form>
      </div>
  	</div>
    <div id="infosWrapper" class="row">
      <div class="col-md-1">
        <img src="images/info_icon.png">
      </div>
      <div class="col-md-11">
        <ul class="nav nav-tabs mt-4" id="infos" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="sobre-tab" data-toggle="tab" href="#sobre" role="tab" aria-controls="sobre" aria-selected="true">sobre</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="fonte_de_dados-tab" data-toggle="tab" href="#fonte_de_dados" role="tab" aria-controls="fonte_de_dados" aria-selected="false">fonte de dados</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="download-tab" data-toggle="tab" href="#download" role="tab" aria-controls="download" aria-selected="false">download</a>
          </li>
        </ul>
        <div class="tab-content mt-4" id="infosContent">
          <div class="tab-pane fade show active" id="sobre" role="tabpanel" aria-labelledby="sobre-tab">
            <?=$infos['sobre']?>
          </div>
          <div class="tab-pane fade show" id="fonte_de_dados" role="tabpanel" aria-labelledby="fonte_de_dados-tab">
            <?=$infos['fonteDeDados']?>
          </div>
          <div class="tab-pane fade" id="download" role="tabpanel" aria-labelledby="download-tab">
            <?=$infos['download']?>
          </div>
        </div>
      </div>
    </div>
  <?php } else { ?>
    <div class="row h-100">
      <div class="col-md-12 h-100">
        <iframe id="iFrame" src="<?=$iframeUrl?>" frameborder="0" allowtransparency></iframe>
      </div>
    </div>
  <?php } ?>
</div>

<?php if ($home) { ?>
  <div class="container-fluid mt-4">
  	<div id="footer" class="row pt-4 footer">
      <?php if (!isset($mainLogo2)): ?>
        <div class="col-md-7 offset-md-1">
          <a href="<?=$mainLink?>" target="_blank">
            <img src="<?=$mainLogo?>" />
          </a>
        </div>
      <?php else: ?>
        <div class="col-md-3 offset-md-1">
          <a href="<?=$mainLink?>" target="_blank">
          <img src="<?=$mainLogo?>" />
          </a>
        </div>
        <div class="col-md-3 offset-md-1">
          <a href="<?=$mainLink2?>" target="_blank">
          <img src="<?=$mainLogo2?>" />
          </a>
        </div>
      <?php endif ?>
      <?php if ($social): ?>
        <div class="col-md-4 text-right my-auto">
          <a href="<?=$webSite?>">Site</a> | <a href="<?=$facebook?>">Facebook</a> | <a href="<?=$youtube?>">Youtube</a>
        </div>
      <?php endif ?>
  	</div>
  </div>
<?php } ?>
</body>
</html>
