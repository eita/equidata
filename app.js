var $input;
$(function () {
  $input = $(".typeahead");
  $inputId = $("#inputId");
  $input.focus(function() {
    $input.val("");
    $inputId.val("");
  });
  $input.typeahead({
    source: source,
    autoSelect: true,
    name: 'dataTypeahead'
  });
  $input.change(function() {
    var current = $input.typeahead("getActive");
    if (current) {
      // Some item from your model is active!
      if (current.name == $input.val()) {
        $input.val(current.name);
        $inputId.val(current.id);
        $("#searchForm").submit();
      } else {
        // This means it is only a partial match, you can either add a new item
        // or take the active if you don't want new items
      }
    } else {
      // Nothing is active so it is a new value (or maybe empty value)
    }
  });

});
